package box;

public class Box {
    private double width;/**Ширина */
    private double height;/**Высота */
    private double depth;/**Глубина */

    public double capasity() {
        return (height + width + depth);
    }

    public Box() {
        width = width;
        height = height;
        depth = depth;
    }


    public Box(double width, double height, double depth) {/**Имя конструктора должно совподать с именем класса.*/
        this.width = width;
        this.height = height;
        this.depth = depth;
    }

    @Override
    public String toString() {/**Вызов alt+ins*/
        return "Ширина =" + width +
                ", Высота=" + height +
                ", Глубина=" + depth;
    }
}

